Do you want to limit the amount of people on your server at time in a certain permissions group? Then this is for you!


## Features ##
- Set a maximum amount of online players in a group at time
- Can use any permission system (currently requires [Vault](http://dev.bukkit.org/bukkit-plugins/vault/ "Download Vault"))
- Have unlimited groups
- Group names come from your permission system
- Bypass checks using a permission

### Future Features ###
- You can change the kick message

**Please Report Bugs In The [Issues Section](http://bitbucket.org/codestrip/maxplayers/issues?status=new&status=open "Go to Issues") Of This Site**